package trandiemquynh.demo_clothes.Controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import trandiemquynh.demo_clothes.Database.SqlManager;
import trandiemquynh.demo_clothes.Model.Clothes;
import trandiemquynh.demo_clothes.R;

//import org.opencv.android.OpenCVLoader;


public class MainActivity extends AppCompatActivity implements ValueEventListener, NavigationView.OnNavigationItemSelectedListener{
    private Firebase mRef;
    SqlManager db = new SqlManager(this);
    DatabaseReference userRef, rootRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        getSupportActionBar().setTitle("Clothing management");
        ArrayList<Clothes> listClothes = db.getAllClothes();
        mRef = new Firebase("https://quynh-cm.firebaseio.com/user/barcode");
        mRef.addValueEventListener(this);

        //database reference pointing to root of database
        rootRef = FirebaseDatabase.getInstance().getReference();
        //database reference pointing to barcode node
        userRef = rootRef.child("user");

//        btnMatchClothes.setOnClickListener(this);
//        btnManageClothes.setOnClickListener(this);

//        if(OpenCVLoader.initDebug()){
//            Log.d("trandiemquynh", "OpenCV loaded successfully");
//        }else{
//            Log.d("trandiemquynh", "OpenCV loaded fail");
//        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(MainActivity.this, AddClothes.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void getFirebaseData(final DataSnapshot dataSnapshot){

        SharedPreferences.Editor editor = getSharedPreferences("My_pref", MODE_PRIVATE).edit();
        SharedPreferences prefs = getSharedPreferences("My_pref", MODE_PRIVATE);
        String temp = prefs.getString("temp", "");
        String barcode = prefs.getString("barcode", "");

        temp = dataSnapshot.getValue(String.class);
        editor.putString("temp", temp);
//        Toast.makeText(getApplicationContext(), temp, Toast.LENGTH_LONG).show();
        editor.commit();

        if(!temp.equals(barcode)){
            barcode = dataSnapshot.getValue(String.class);
            editor.putString("barcode", barcode);
            editor.commit();
            if(!barcode.equals("0")) {
                Toast.makeText(getApplicationContext(), "Barcode: " + barcode, Toast.LENGTH_SHORT).show();
            }
            barcodeControl(barcode);
        } else if(temp==null || barcode==null){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_SHORT).show();
        }
    }

    public void barcodeControl(String barcode){
        Clothes cloth;
        cloth = db.getClothById(barcode);
        if(cloth.getStatus()!=null){
            Integer status = cloth.getStatus();

            if (status == 1) {
                db.setId_0(barcode);
                cloth = db.getClothById(barcode);
                Toast.makeText(getApplicationContext(), cloth.getName() + " of " + cloth.getBrand() + " isn't in the wardrobe!", Toast.LENGTH_LONG).show();
            } else {
                db.setId_1(barcode);
                cloth = db.getClothById(barcode);
                Toast.makeText(getApplicationContext(), cloth.getName() + " of " + cloth.getBrand() + " is in the wardrobe!", Toast.LENGTH_LONG).show();
            }

        }
        if(!barcode.equals("0") && cloth.getStatus()==null){
            Log.e("aa", "barcodeControl: "+ barcode);
             Exception(barcode);
        }
        //push creates a unique id in database
        userRef.child("barcode").setValue("0");
    }

    public void Exception(final String barcode){
        AlertDialog.Builder b =  new AlertDialog.Builder(MainActivity.this);
        b.setTitle("Wrong barcode!");
        b.setMessage("Barcode: "+barcode+" doesn't exist! \nDo you want to scan barcode again?");
        b.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        b.setNegativeButton("No", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){

            }});
        AlertDialog dlg = b.create();
       /* b.setCancelable(false);
        dlg.setCancelable(false);
        dlg.setCanceledOnTouchOutside(false);*/
        dlg.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dlg.show();
    }
    @Override
    public void onDataChange(final DataSnapshot dataSnapshot) {
        getFirebaseData(dataSnapshot);

    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {

    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_mixmatch) {
            Intent intent = new Intent(MainActivity.this, MatchClothes.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_manage) {
            Intent intent = new Intent(MainActivity.this, ManageClothes.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_outfit) {
            Intent intent = new Intent(MainActivity.this, Outfit.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(MainActivity.this, About.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_instruction) {
            Intent intent = new Intent(MainActivity.this, Instruction.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_items) {
            Intent intent = new Intent(MainActivity.this, ItemsList.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
