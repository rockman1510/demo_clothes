package trandiemquynh.demo_clothes.Controller;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.regex.Pattern;

import trandiemquynh.demo_clothes.Database.SqlManager;
import trandiemquynh.demo_clothes.Model.Clothes;
import trandiemquynh.demo_clothes.R;

public class UpdateClothes extends AppCompatActivity{

    private EditText etID, etName, etDescription, etBrand, etStatus;
    SqlManager sql = new SqlManager(this);
    ImageView imageViewUpdate;
    private Uri mImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_clothes);
        getSupportActionBar().setTitle("Update item");
//        ActionBar mActionBar = getSupportActionBar();
//        mActionBar.setDisplayShowHomeEnabled(false);
//        mActionBar.setDisplayShowTitleEnabled(false);

        etID = (EditText)findViewById(R.id.etID);
        etName = (EditText)findViewById(R.id.etName);
        etDescription = (EditText)findViewById(R.id.etDescription);
        etBrand = (EditText)findViewById(R.id.etBrand);
        etStatus = (EditText)findViewById(R.id.etStatus);
        imageViewUpdate = (ImageView) findViewById(R.id.imageViewUpdate);

        // Get String arrays FilePathStrings

        Intent i = getIntent();
        etID.setText(i.getExtras().getString("id"));
        etName.setText(i.getExtras().getString("name"));
        etDescription.setText(i.getExtras().getString("des"));
        etBrand.setText(i.getExtras().getString("brand"));
        etStatus.setText(i.getExtras().getString("stt"));
        Retrieve(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/"+etID.getText()+".jpg");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(UpdateClothes.this,ManageClothes.class);
        startActivity(intent);
    }

    public void Retrieve(String filepath)
    {
        File imageFile = new File(filepath);
        if(imageFile.exists()){
            Bitmap image = BitmapFactory.decodeFile(filepath);
            // Rotate Image if Needed
            try{
                // Determine Orientation
                ExifInterface exif = new ExifInterface(filepath);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                // Determine Rotation
                int rotation = 0;
                if      (orientation == 6)      rotation = 90;
                else if (orientation == 3)      rotation = 180;
                else if (orientation == 8)      rotation = 270;
                // Rotate Image if Necessary
                if (rotation != 0){
                    // Create Matrix
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    // Rotate Bitmap
                    Bitmap rotated = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
                    // Pretend none of this ever happened!
                    image.recycle();
                    image = rotated;
                    rotated = null;
                }
            }
            catch (Exception e){}
            imageViewUpdate.setImageBitmap(image);
        }
        else Toast.makeText(this, "You haven't added photo for this item", Toast.LENGTH_SHORT).show();
    }

    public void doClickSave(){
        Clothes clothes = sql.getClothByName(etName.getText().toString());
        Pattern regex = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!]");
        if(etName.getText().toString().trim()!=null && !etName.getText().toString().trim().equals("") && !etName.getText().toString().trim().isEmpty()){
            String id = etID.getText().toString();
            String name = etName.getText().toString();
            String description = etDescription.getText().toString();
            String brand = etBrand.getText().toString();
//            if(name.equalsIgnoreCase(clothes.getName()) &&
//                    description.equalsIgnoreCase(clothes.getDescription()) &&
//                    brand.equalsIgnoreCase(clothes.getBrand()))
//            {
//                Toast.makeText(UpdateClothes.this, "This item's information has already been in the database", Toast.LENGTH_SHORT).show();
            if(regex.matcher(name).find()){
                Toast.makeText(UpdateClothes.this, "'Name' mustn't include special characters", Toast.LENGTH_SHORT).show();
            } else if(regex.matcher(description).find()) {
                Toast.makeText(UpdateClothes.this, "'Description' mustn't include special characters", Toast.LENGTH_SHORT).show();
            } else {
                sql.updateClothes(id, name, description, brand);
                Toast.makeText(getApplicationContext(), "Updated successfully", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(UpdateClothes.this,ManageClothes.class);
                startActivity(intent);
            }
        } else {
            Toast.makeText(UpdateClothes.this, "Please type in 'Name'", Toast.LENGTH_SHORT).show();
        }
    }
    public void doClickTakePhoto(){
        //To take picture from camera:
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try
        {
            File imagesFolder = new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes");
            imagesFolder.mkdirs();
            photo = new File(imagesFolder,etID.getText()+".jpg");
        }
        catch(Exception e){
        }

        mImageUri = Uri.fromFile(photo);
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        //start camera intent
        startActivityForResult(takePicture, 1);
    }

    public void grabImage(ImageView imageView)
    {
        this.getContentResolver().notifyChange(mImageUri, null);
        ContentResolver cr = this.getContentResolver();
        Bitmap bitmap;
        try
        {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
            imageView.setImageBitmap(bitmap);
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    this.grabImage(imageViewUpdate);
                    imageViewUpdate.setRotation(90);
                    imageViewUpdate.setScaleType(ImageView.ScaleType.FIT_CENTER);
                }
                break;
        }
    }

    // tao button tren action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.updateclothes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // xu ly button tren action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.btnExit){
            doClickExit();
        } else if(id == R.id.btnBack){
            doClickBack();
        }else if(id == R.id.btnSave){
            doClickSave();
        } else if(id == R.id.btnTakePhoto){
            doClickTakePhoto();
        }
        return super.onOptionsItemSelected(item);
    }

    public void doClickBack(){
        Intent intent = new Intent(UpdateClothes.this,ManageClothes.class);
        startActivity(intent);
        finish();
    }
    public void doClickExit(){
        Intent intent = new Intent(UpdateClothes.this,MainActivity.class);
        startActivity(intent);
        finish();
    }



}
