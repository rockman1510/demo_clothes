package trandiemquynh.demo_clothes.Controller;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import trandiemquynh.demo_clothes.Others.GridViewImagesList;
import trandiemquynh.demo_clothes.R;

//import org.opencv.android.OpenCVLoader;

public class MatchClothes extends AppCompatActivity implements View.OnTouchListener, AdapterView.OnItemSelectedListener {
    private ImageView imageView1;
    private ImageView imageView2;
    private TextView txtTop, txtBottom;
    private boolean top = false;
    private boolean bottom = false;
    private Spinner spinner;
    private static final String[]paths = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    private boolean selected = false;

    // these matrices will be used to move and zoom image
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_clothes);
        getSupportActionBar().setTitle("Mix-Match");

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        txtTop = (TextView) findViewById(R.id.txtTop);
        txtBottom = (TextView) findViewById(R.id.txtBottom);
        spinner = (Spinner) findViewById(R.id.spinner);

        imageView1.setOnTouchListener(this);
        imageView2.setOnTouchListener(this);

        ArrayAdapter<String>adapter = new ArrayAdapter<String>(MatchClothes.this, android.R.layout.simple_spinner_item,paths);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MatchClothes.this,MainActivity.class);
        startActivity(intent);
    }

    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height
                        / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // handle touch events here
        ImageView view = (ImageView) v;

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                mode = DRAG;
                lastEvent = null;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                }
                lastEvent = new float[4];
                lastEvent[0] = event.getX(0);
                lastEvent[1] = event.getX(1);
                lastEvent[2] = event.getY(0);
                lastEvent[3] = event.getY(1);
                d = rotation(event);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                lastEvent = null;
                break;
            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {
                    matrix.set(savedMatrix);
                    float dx = event.getX() - start.x;
                    float dy = event.getY() - start.y;
                    matrix.postTranslate(dx, dy);
                } else if (mode == ZOOM) {
                    float newDist = spacing(event);
                    if (newDist > 10f) {
                        matrix.set(savedMatrix);
                        float scale = (newDist / oldDist);
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                    if (lastEvent != null && event.getPointerCount() == 3) {
                        newRot = rotation(event);
                        float r = newRot - d;
                        float[] values = new float[9];
                        matrix.getValues(values);
                        float tx = values[2];
                        float ty = values[5];
                        float sx = values[0];
                        float xc = (view.getWidth() / 2) * sx;
                        float yc = (view.getHeight() / 2) * sx;
                        matrix.postRotate(r, tx + xc, ty + yc);
                    }
                }
                break;
        }

        view.setImageMatrix(matrix);
        return true;
    }

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /**
     * Calculate the degree to be rotated by.
     *
     * @param event
     * @return Degrees
     */
    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }
    protected void selectPhoto1(){
        //To pick photo from gallery:
//        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(pickPhoto , 1);

        File file= new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/");
        if(file.exists()){
            Intent intent = new Intent(MatchClothes.this,GridViewImagesList.class);
            startActivityForResult(intent, 1);
        } else Toast.makeText(this, "Please add clothes' images to gallery", Toast.LENGTH_SHORT).show();

    }

    protected void selectPhoto2(){
        File file= new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/");
        if(file.exists()) {
            Intent intent = new Intent(MatchClothes.this, GridViewImagesList.class);
            startActivityForResult(intent, 2);
        } else Toast.makeText(this, "Please add clothes' images to gallery", Toast.LENGTH_SHORT).show();

    }

    private Uri mImageUri;
    protected void takePicture1(){
        //To take picture from camera:
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try
        {
            String timeStamp = new SimpleDateFormat("ddMMyyyy").format(new Date());
            // place where to store camera taken picture
            File imagesFolder = new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/ClothesFromStore");
            imagesFolder.mkdirs();
            photo = new File(imagesFolder,"Photo_"+timeStamp+".jpg");
        }
        catch(Exception e){
        }
        mImageUri = Uri.fromFile(photo);
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(takePicture, 3);

    }

    protected void takePicture2(){
        //To take picture from camera:
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try
        {
            String timeStamp = new SimpleDateFormat("ddMMyyyy").format(new Date());
            // place where to store camera taken picture
            File imagesFolder = new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/ClothesFromStore");
            imagesFolder.mkdirs();
            photo = new File(imagesFolder,"Photo_"+timeStamp+".jpg");
        }
        catch(Exception e){
        }
        mImageUri = Uri.fromFile(photo);
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        //start camera intent
        startActivityForResult(takePicture, 4);
    }

    public void grabImage(ImageView imageView)
    {
        this.getContentResolver().notifyChange(mImageUri, null);
        ContentResolver cr = this.getContentResolver();
        Bitmap bitmap;
        try
        {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);

            imageView.setImageBitmap(bitmap);
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
        }
    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
//                    Uri selectedImage = imageReturnedIntent.getData();
//                    imageView2.setImageURI(selectedImage);
//                    imageView2.setVisibility(View.VISIBLE);

                    int position = imageReturnedIntent.getExtras().getInt("position");
                    // Get String arrays FilePathStrings
                    String[] filepath = imageReturnedIntent.getStringArrayExtra("filepath");
                    Bitmap image = decodeSampledBitmapFromUri(filepath[position], 250, 250);

                    try{
                        // Determine Orientation
                        ExifInterface exif = new ExifInterface(filepath[position]);
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                        // Determine Rotation
                        int rotation = 0;
                        if      (orientation == 6)      rotation = 90;
                        else if (orientation == 3)      rotation = 180;
                        else if (orientation == 8)      rotation = 270;
                        // Rotate Image if Necessary
                        if (rotation != 0){
                            // Create Matrix
                            Matrix matrix = new Matrix();
                            matrix.postRotate(rotation);
                            // Rotate Bitmap
                            Bitmap rotated = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
                            // Pretend none of this ever happened!
                            image.recycle();
                            image = rotated;
                            rotated = null;
                        }
                    }
                    catch (Exception e){}

                    // Set the decoded bitmap into ImageView
                    imageView1.setImageBitmap(image);
                    imageView1.setVisibility(View.VISIBLE);
                    txtTop.setVisibility(View.GONE);
                    top = true;
                }
                break;

            case 2:
                if(resultCode == RESULT_OK){
                        int position = imageReturnedIntent.getExtras().getInt("position");
                        // Get String arrays FilePathStrings
                        String[] filepath = imageReturnedIntent.getStringArrayExtra("filepath");
                        Bitmap image = decodeSampledBitmapFromUri(filepath[position], 250, 250);

                        try{
                            // Determine Orientation
                            ExifInterface exif = new ExifInterface(filepath[position]);
                            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                            // Determine Rotation
                            int rotation = 0;
                            if      (orientation == 6)      rotation = 90;
                            else if (orientation == 3)      rotation = 180;
                            else if (orientation == 8)      rotation = 270;
                            // Rotate Image if Necessary
                            if (rotation != 0){
                                // Create Matrix
                                Matrix matrix = new Matrix();
                                matrix.postRotate(rotation);
                                // Rotate Bitmap
                                Bitmap rotated = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
                                // Pretend none of this ever happened!
                                image.recycle();
                                image = rotated;
                                rotated = null;
                            }
                        }
                        catch (Exception e){}

                        // Set the decoded bitmap into ImageView
                        imageView2.setImageBitmap(image);
                        imageView2.setVisibility(View.VISIBLE);
                        txtBottom.setVisibility(View.GONE);
                        bottom = true;
                }
                break;

            case 3:
                if(resultCode == RESULT_OK){
                    this.grabImage(imageView1);
                    imageView1.setRotation(90);
                    imageView1.setVisibility(View.VISIBLE);
                    txtTop.setVisibility(View.GONE);
                    top = true;
                }
                break;
            case 4:
                if(resultCode == RESULT_OK){
                    this.grabImage(imageView2);
                    imageView2.setRotation(90);
                    imageView2.setVisibility(View.VISIBLE);
                    txtBottom.setVisibility(View.GONE);
                    bottom = true;
                }
                break;
        }
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            File imagesFolder = new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Outfit");
            imagesFolder.mkdirs();

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(imagesFolder,now+".jpg");

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

    // tao button tren action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.matchclothes, menu);
        return super.onCreateOptionsMenu(menu);
    }
    // xu ly button tren action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.btnExit){
            doClickExit();
        } else if(id == R.id.btnSave) {
            if (top == true && bottom == true) {
                takeScreenshot();
                Toast.makeText(this, "Your mix-match has been saved", Toast.LENGTH_SHORT).show();
            } else if(selected == false) {
                Toast.makeText(this, "Select when you wear this outfit in 'Title'", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Choose item to mix and match", Toast.LENGTH_SHORT).show();
            }
        } else if(id == R.id.btnSelectPhoto){
            AlertDialog.Builder b =  new AlertDialog.Builder(MatchClothes.this);
            b.setTitle("Gallery");
            b.setMessage("Choose image from Gallery");
            b.setPositiveButton("Bottom", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    selectPhoto2();
                }
            });
            b.setNegativeButton("Top", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                    selectPhoto1();
                }});
            b.create().show();
        }
        else if(id == R.id.btnTakePhoto){
            AlertDialog.Builder b =  new AlertDialog.Builder(MatchClothes.this);
            b.setTitle("Camera");
            b.setMessage("Take photo");
            b.setPositiveButton("Bottom", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    takePicture2();
                }
            });
            b.setNegativeButton("Top", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                    takePicture1();
                }});
            b.create().show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void doClickExit(){
        Intent intent = new Intent(MatchClothes.this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selected = true;
        ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
        ((TextView) parent.getChildAt(0)).setTextSize(22);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(this, "Choose when you wear this cloth in 'Title'", Toast.LENGTH_SHORT).show();
    }
}