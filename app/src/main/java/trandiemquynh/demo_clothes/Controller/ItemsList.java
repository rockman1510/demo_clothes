package trandiemquynh.demo_clothes.Controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;

import trandiemquynh.demo_clothes.Database.SqlManager;
import trandiemquynh.demo_clothes.Model.Clothes;
import trandiemquynh.demo_clothes.Others.GridViewItemsList;
import trandiemquynh.demo_clothes.R;

public class ItemsList extends AppCompatActivity {
    // Declare variables
    private String[] FilePathStrings;
    private String[] FileNameStrings;
    private File[] listFile;
    private String[] status;
    private GridView grid;
    private GridViewItemsList adapter;
    private File file;
    private SqlManager db = new SqlManager(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_image_grid_view);
        getSupportActionBar().setTitle("Items' Images");
        file = new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/");

        if (file.isDirectory()) {
            listFile = file.listFiles();
            // Create a String array for FilePathStrings
            FilePathStrings = new String[listFile.length];
            // Create a String array for FileNameStrings
            FileNameStrings = new String[listFile.length];

            status = new String[listFile.length];


            for (int i = 0; i < listFile.length; i++) {
                // Get the path of the image file
                FilePathStrings[i] = listFile[i].getAbsolutePath();
                // Get the name image file
                FileNameStrings[i] = listFile[i].getName();

                String[] parts = FileNameStrings[i].split(Pattern.quote("."));
                String filename = parts[0]; //file name
                String part2 = parts[1];
//                Toast.makeText(this, filename+"", Toast.LENGTH_SHORT).show();
                Clothes cloth = db.getClothById(filename);
                if(cloth.getStatus()!=null) {
                    status[i] = cloth.getStatus().toString();
//                Toast.makeText(this, cloth.getStatus()+"", Toast.LENGTH_SHORT).show();
                } else status[i]=String.valueOf(0);

            }
        }

        grid = (GridView) findViewById(R.id.gridview);
        // Pass String arrays to GridViewAdapter Class
        adapter = new GridViewItemsList(this, FilePathStrings, FileNameStrings, status);


        // Set the GridViewAdapter to the GridViewImagesList
        grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ArrayList<Clothes> listClothes = db.getAllClothes();
                Clothes cloth = listClothes.get(position);
                Intent intent = new Intent(ItemsList.this, ShowClothes.class);
                intent.putExtra("Data",cloth);
                startActivity(intent);
                finish();
            }

        });

//        grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view,
//                                           final int position, long id) {
//                AlertDialog.Builder b =  new AlertDialog.Builder(ItemsList.this);
//                b.setTitle("Delete Item!");
//                b.setMessage("Do you want to delete this item?");
//                b.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        doClickDelete(position);
//                        Intent intent = new Intent(ItemsList.this,ItemsList.class);
//                        startActivity(intent);
//                    }
//                });
//                b.setNegativeButton("No", new DialogInterface.OnClickListener(){
//                    @Override
//                    public void onClick(DialogInterface dialog, int which){
//
//                    }});
//                b.create().show();
//                return true;
//            }
//
//        });

    }

//    public void doClickDelete(int position){
//        File file= new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/"+FileNameStrings[position]);
//        if(file.exists())
//        {
//            file.delete();
//        }
//        Toast.makeText(this, "Deleted successfully!", Toast.LENGTH_SHORT).show();
//        finish();
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ItemsList.this,MainActivity.class);
        startActivity(intent);
    }


    // tao button tren action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back, menu);
        return super.onCreateOptionsMenu(menu);
    }
    // xu ly button tren action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.btnExit){
            doClickExit();
        }
        return super.onOptionsItemSelected(item);
    }

    public void doClickExit(){
        Intent intent = new Intent(ItemsList.this,MainActivity.class);
        startActivity(intent);
        finish();
    }

}
