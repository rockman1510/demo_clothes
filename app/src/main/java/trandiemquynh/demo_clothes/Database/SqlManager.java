package trandiemquynh.demo_clothes.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.widget.Toast;

import java.util.ArrayList;

import trandiemquynh.demo_clothes.Model.Clothes;

/**
 * Created by Admin on 26-Sep-17.
 */

public class SqlManager extends SQLiteOpenHelper{
    private Context context;// ngu canh hien tai
    private SQLiteDatabase sqLiteDatabase;// thao tac voi CSDL sqliteDatabase.
    public static final String TABLE_NAME = "tblClothes";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_Name = "name";
    public static final String COLUMN_Description = "description";
    public static final String COLUMN_Brand = "brand";
    public static final String COLUMN_Status = "status";

    public static final String PATH = Environment.getDataDirectory() + "/data/trandiemquynh.demo_clothes/databases";// tro vao thu muc data/data/tenUngdung/database.
    public static final String DATABASE_NAME = "ClothesDatabase.db";

    public SqlManager(Context context) {
        super(context, DATABASE_NAME,null, 1);
        this.context = context;
       /* try {
            writeDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlQuery = "CREATE TABLE "+TABLE_NAME +" (" +
                COLUMN_ID +" TEXT primary key, "+
                COLUMN_Name + " TEXT, "+
                COLUMN_Description +" TEXT, "+
                COLUMN_Brand+" TEXT," +
                COLUMN_Status +" INTEGER)";
        db.execSQL(sqlQuery);
        Toast.makeText(context, "Create Clothes Database successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
        Toast.makeText(context, "Drop Clothes Database successfully", Toast.LENGTH_SHORT).show();
    }

//    private void writeDatabase() throws Exception {
//        File file = new File(PATH + "/" + DATABASE_NAME);
//        if (!file.exists()) {
//            File fileParent = new File(PATH);// tao thu muc database trc' sau do moi tao thu muc AI_LA...sqlite.
//            if (!fileParent.exists()) {
//                fileParent.mkdirs();
//            }
//            file.createNewFile();// bat ngoai le o day.
//            FileOutputStream fileOutputStream = new FileOutputStream(file);
//            InputStream inputStream = context.getAssets().open(DATABASE_NAME);// mo ra luong cua 1 file trong thu muc asset
//            byte[] b = new byte[1024];
//            int count = inputStream.read(b);// so byte doc dc trong cau lenh read.
//            while (count != -1) {
//                fileOutputStream.write(b, 0, count);
//                count = inputStream.read(b);
//            }
//            fileOutputStream.close();
//            inputStream.close();
//        }
//    }

    private void openDatabase() {
        sqLiteDatabase = context.openOrCreateDatabase(PATH + "/" + DATABASE_NAME, context.MODE_PRIVATE, null);//open Database AI_la...
    }

    private void closeDatabase() {
        sqLiteDatabase.close();
    }

    public void addClothes(Clothes cloth){
        openDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put(COLUMN_ID,cloth.getId().toLowerCase());
        contentValues.put(COLUMN_Name,cloth.getName().toLowerCase());
        contentValues.put(COLUMN_Description,cloth.getDescription().toLowerCase());
        contentValues.put(COLUMN_Brand,cloth.getBrand().toLowerCase());
        contentValues.put(COLUMN_Status,cloth.getStatus());

        sqLiteDatabase.insert(TABLE_NAME,null,contentValues);
        closeDatabase();// remember close
    }



    public ArrayList<Clothes> getAllClothes(){
        ArrayList<Clothes> listClothes = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        //openDatabase();
        Cursor cursor= db.rawQuery("SELECT * FROM " + TABLE_NAME,null);

        if(cursor.moveToFirst()){ // doc dong dau tien.

            do{
                Clothes w = new Clothes();
                String id = cursor.getString(0);
                String name = cursor.getString(1);
                String description = cursor.getString(2);
                String brand = cursor.getString(3);
                int status = cursor.getInt(4);

                w.setId(id);
                w.setName(name);
                w.setDescription(description);
                w.setBrand(brand);
                w.setStatus(status);

                listClothes.add(w);

            }while (cursor.moveToNext()); // chuyen toi ban ghi o dong tiep theo.
        }
        cursor.close();
        db.close();

        return listClothes;
    }

    public Clothes getClothById(String value) {
        Clothes w = new Clothes();
        openDatabase();//mo Database de thao tac.
//        SQLiteDatabase db = this.getWritableDatabase();

//        Cursor cursor = sqLiteDatabase.rawQuery("SELECT id, word, meaning FROM tblWord WHERE meaning='"+meaningF+"'", null);// cach khac de lay tat ca truong trong table
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM tblClothes WHERE id='"+value+"'", null);// cach khac de lay tat ca truong trong table

        cursor.moveToFirst();// doc thang dau tien.
        int indexId = cursor.getColumnIndex(COLUMN_ID);// tra ve vi tri cot_ID
        int indexName = cursor.getColumnIndex(COLUMN_Name);
        int indexDescription = cursor.getColumnIndex(COLUMN_Description);
        int indexBrand = cursor.getColumnIndex(COLUMN_Brand);
        int indexStatus = cursor.getColumnIndex(COLUMN_Status);

        while (!cursor.isAfterLast()) {
            String id = cursor.getString(indexId);
            String name = cursor.getString(indexName);
            String description = cursor.getString(indexDescription);
            String brand = cursor.getString(indexBrand);
            Integer status = cursor.getInt(indexStatus);

            w = new Clothes(id, name, description, brand, status);
            cursor.moveToNext();// chuyen toi ban ghi o dong tiep theo.

        }
//        db.close();
        closeDatabase();
        return w;
    }

    public Clothes getClothByName(String value) {
        Clothes w = new Clothes();
        openDatabase();//mo Database de thao tac.
//        SQLiteDatabase db = this.getWritableDatabase();

//        Cursor cursor = sqLiteDatabase.rawQuery("SELECT id, word, meaning FROM tblWord WHERE meaning='"+meaningF+"'", null);// cach khac de lay tat ca truong trong table
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM tblClothes WHERE name='"+value.toLowerCase()+"'", null);// cach khac de lay tat ca truong trong table

        cursor.moveToFirst();// doc thang dau tien.
        int indexId = cursor.getColumnIndex(COLUMN_ID);// tra ve vi tri cot_ID
        int indexName = cursor.getColumnIndex(COLUMN_Name);
        int indexDescription = cursor.getColumnIndex(COLUMN_Description);
        int indexBrand = cursor.getColumnIndex(COLUMN_Brand);
        int indexStatus = cursor.getColumnIndex(COLUMN_Status);

        while (!cursor.isAfterLast()) {
            String id = cursor.getString(indexId);
            String name = cursor.getString(indexName);
            String description = cursor.getString(indexDescription);
            String brand = cursor.getString(indexBrand);
            Integer status = cursor.getInt(indexStatus);

            w = new Clothes(id, name, description, brand, status);
            cursor.moveToNext();// chuyen toi ban ghi o dong tiep theo.

        }
//        db.close();
        closeDatabase();
        return w;
    }

    public Clothes setId_0 (String barcode) {
        Clothes w = new Clothes();
        openDatabase();//mo Database de thao tac.

        Cursor cursor = sqLiteDatabase.rawQuery("UPDATE tblClothes SET status = '0' WHERE id ='"+barcode+"'", null);// cach khac de lay tat ca truong trong table

        cursor.moveToFirst();// doc thang dau tien.
        int indexId = cursor.getColumnIndex(COLUMN_ID);// tra ve vi tri cot_ID
        int indexName = cursor.getColumnIndex(COLUMN_Name);
        int indexDescription = cursor.getColumnIndex(COLUMN_Description);
        int indexBrand = cursor.getColumnIndex(COLUMN_Brand);
        int indexStatus = cursor.getColumnIndex(COLUMN_Status);

        while (!cursor.isAfterLast()) {
            String id = cursor.getString(indexId);
            String name = cursor.getString(indexName);
            String description = cursor.getString(indexDescription);
            String brand = cursor.getString(indexBrand);
            Integer status = cursor.getInt(indexStatus);

            w = new Clothes(id, name, description, brand, status);
            cursor.moveToNext();// chuyen toi ban ghi o dong tiep theo.

        }
        closeDatabase();
        return w;
    }

    public Clothes setId_1 (String barcode) {
        Clothes w = new Clothes();
        openDatabase();//mo Database de thao tac.

        Cursor cursor = sqLiteDatabase.rawQuery("UPDATE tblClothes SET status = '1' WHERE id ='"+barcode+"'", null);// cach khac de lay tat ca truong trong table

        cursor.moveToFirst();// doc thang dau tien.
        int indexId = cursor.getColumnIndex(COLUMN_ID);// tra ve vi tri cot_ID
        int indexName = cursor.getColumnIndex(COLUMN_Name);
        int indexDescription = cursor.getColumnIndex(COLUMN_Description);
        int indexBrand = cursor.getColumnIndex(COLUMN_Brand);
        int indexStatus = cursor.getColumnIndex(COLUMN_Status);

        while (!cursor.isAfterLast()) {
            String cid = cursor.getString(indexId);
            String cname = cursor.getString(indexName);
            String cdescription = cursor.getString(indexDescription);
            String cbrand = cursor.getString(indexBrand);
            Integer cstatus = cursor.getInt(indexStatus);

            w = new Clothes(cid,cname,cdescription,cbrand,cstatus);
            cursor.moveToNext();// chuyen toi ban ghi o dong tiep theo.

        }
        closeDatabase();
        return w;
    }

    public Clothes updateClothes (String barcode, String name, String description, String brand) {
        Clothes w = new Clothes();
        openDatabase();//mo Database de thao tac.

        Cursor cursor = sqLiteDatabase.rawQuery("UPDATE tblClothes SET name = '"+name+"'," +
                                           "description='"+description+"'," +
                                            "brand='"+brand+"' WHERE id ='"
                                         +barcode+"'", null);// cach khac de lay tat ca truong trong table

        cursor.moveToFirst();// doc thang dau tien.
        int indexId = cursor.getColumnIndex(COLUMN_ID);// tra ve vi tri cot_ID
        int indexName = cursor.getColumnIndex(COLUMN_Name);
        int indexDescription = cursor.getColumnIndex(COLUMN_Description);
        int indexBrand = cursor.getColumnIndex(COLUMN_Brand);
        int indexStatus = cursor.getColumnIndex(COLUMN_Status);

        while (!cursor.isAfterLast()) {
            String cid = cursor.getString(indexId);
            String cname = cursor.getString(indexName);
            String cdescription = cursor.getString(indexDescription);
            String cbrand = cursor.getString(indexBrand);
            Integer cstatus = cursor.getInt(indexStatus);

            w = new Clothes(cid,cname,cdescription,cbrand,cstatus);
            cursor.moveToNext();// chuyen toi ban ghi o dong tiep theo.

        }
        closeDatabase();
        return w;
    }

    public Clothes deleteCloth (String barcode) {
        Clothes w = new Clothes();
        openDatabase();//mo Database de thao tac.

        Cursor cursor = sqLiteDatabase.rawQuery("DELETE FROM tblClothes WHERE id ='"+barcode+"'", null);

        cursor.moveToFirst();// doc thang dau tien.
        int indexId = cursor.getColumnIndex(COLUMN_ID);// tra ve vi tri cot_ID
        int indexName = cursor.getColumnIndex(COLUMN_Name);
        int indexDescription = cursor.getColumnIndex(COLUMN_Description);
        int indexBrand = cursor.getColumnIndex(COLUMN_Brand);
        int indexStatus = cursor.getColumnIndex(COLUMN_Status);

        while (!cursor.isAfterLast()) {
            String cid = cursor.getString(indexId);
            String cname = cursor.getString(indexName);
            String cdescription = cursor.getString(indexDescription);
            String cbrand = cursor.getString(indexBrand);
            Integer cstatus = cursor.getInt(indexStatus);

            w = new Clothes(cid,cname,cdescription,cbrand,cstatus);
            cursor.moveToNext();// chuyen toi ban ghi o dong tiep theo.

        }
        closeDatabase();
        return w;
    }
}
