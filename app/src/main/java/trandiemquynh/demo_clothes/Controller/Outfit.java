package trandiemquynh.demo_clothes.Controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;

import trandiemquynh.demo_clothes.Database.SqlManager;
import trandiemquynh.demo_clothes.Others.GridViewOutfit;
import trandiemquynh.demo_clothes.R;

public class Outfit extends AppCompatActivity {
    // Declare variables
    private String[] FilePathStrings;
    private String[] FileNameStrings;
    private File[] listFile;
    private String[] status;
    GridView grid;
    GridViewOutfit gridView;
    File file;
    SqlManager db = new SqlManager(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_image_grid_view);
        getSupportActionBar().setTitle("Outfit");
        file = new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Outfit/");

        if (file.isDirectory()) {
            listFile = file.listFiles();
            // Create a String array for FilePathStrings
            FilePathStrings = new String[listFile.length];
            // Create a String array for FileNameStrings
            FileNameStrings = new String[listFile.length];

            status = new String[listFile.length];


            for (int i = 0; i < listFile.length; i++) {
                // Get the path of the image file
                FilePathStrings[i] = listFile[i].getAbsolutePath();
                // Get the name image file
                FileNameStrings[i] = listFile[i].getName();
            }
        }

        // Locate the ListImageGridView in list_image_grid_view.xml
        grid = (GridView) findViewById(R.id.gridview);

        File file= new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Outfit/");
        if(file.exists()){
            // Pass String arrays to GridViewAdapter Class
            gridView = new GridViewOutfit(this, FilePathStrings, FileNameStrings);
            // Set the GridViewAdapter to the ListImageGridView
            grid.setAdapter(gridView);
        } else Toast.makeText(this, "Please mix and match your clothes' images", Toast.LENGTH_SHORT).show();

        // Capture gridview item click
        grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                    final int position, long id) {
                AlertDialog.Builder b =  new AlertDialog.Builder(Outfit.this);
                b.setTitle("Delete Outfit!");
                b.setMessage("Do you want to delete this outfit?");
                b.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doClickDelete(position);
                        Intent intent = new Intent(Outfit.this,Outfit.class);
                        startActivity(intent);
                    }
                });
                b.setNegativeButton("No", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){

                    }});
                b.create().show();
                return true;
            }

        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Outfit.this,MainActivity.class);
        startActivity(intent);
    }

    public void doClickDelete(int position){
        File file= new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Outfit/"+FileNameStrings[position]);
        if(file.exists())
        {
            file.delete();
        }
        Toast.makeText(this, "Deleted successfully!", Toast.LENGTH_SHORT).show();
        finish();
    }


    // tao button tren action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back, menu);
        return super.onCreateOptionsMenu(menu);
    }
    // xu ly button tren action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.btnExit){
            doClickExit();
        }
        return super.onOptionsItemSelected(item);
    }

    public void doClickExit(){
        Intent intent = new Intent(Outfit.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
