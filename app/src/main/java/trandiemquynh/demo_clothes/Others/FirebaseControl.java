package trandiemquynh.demo_clothes.Others;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import trandiemquynh.demo_clothes.Database.SqlManager;
import trandiemquynh.demo_clothes.Model.Clothes;

import static trandiemquynh.demo_clothes.R.layout.activity_main;

public class FirebaseControl extends AppCompatActivity implements ValueEventListener {
    private Firebase mRef;
    SqlManager db = new SqlManager(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);

        mRef = new Firebase("https://quynh-cm.firebaseio.com/user/barcode");
        mRef.addValueEventListener(this);
    }
        @Override
        public void onDataChange(final DataSnapshot dataSnapshot) {
            String value = dataSnapshot.getValue(String.class);
            //receive barcode from camera scanning
            final String barcode = value;
            Toast.makeText(getApplicationContext(), "Barcode: " + barcode, Toast.LENGTH_SHORT).show();

            Clothes cloth;
            cloth = db.getClothById(barcode);
            Integer status = cloth.getStatus();
            Toast.makeText(getApplicationContext(), "PreStatus: "+status, Toast.LENGTH_SHORT).show();

//        String preStatus = null;
//        if(status == 1){
//            preStatus = "in the wardrobe";
//
//        }
//        else {
//            preStatus = "not in the wardrobe";
//        }
//        Toast.makeText(getApplicationContext(), cloth.getName()+" is " + preStatus, Toast.LENGTH_SHORT).show();

            if (status == 1) {
                db.setId_0(barcode);
                cloth = db.getClothById(barcode);
                Toast.makeText(getApplicationContext(), cloth.getName() + " of " + cloth.getBrand() + " isn't in the wardrobe!", Toast.LENGTH_LONG).show();
            } else  {
                db.setId_1(barcode);
                cloth = db.getClothById(barcode);
                Toast.makeText(getApplicationContext(), cloth.getName() + " of " + cloth.getBrand() + " is in the wardrobe!", Toast.LENGTH_LONG).show();
            }


//        AlertDialog.Builder b = new AlertDialog.Builder(ManageClothes.this);
//        b.setTitle("Item's status");
//        b.setMessage("Where is the "+cloth.getName()+"?");
//        b.setPositiveButton("Not in the wardrobe", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                String value = dataSnapshot.getValue(String.class);
//                db.setId_0(value);
//            }
//        });
//        b.setNegativeButton("In the wardrobe", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                String value = dataSnapshot.getValue(String.class);
//                db.setId_1(value);
//            }
//        });
//        b.create().show();
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }

}
