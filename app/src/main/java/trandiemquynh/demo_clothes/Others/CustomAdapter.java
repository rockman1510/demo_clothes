package trandiemquynh.demo_clothes.Others;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import trandiemquynh.demo_clothes.Model.Clothes;
import trandiemquynh.demo_clothes.R;

public class CustomAdapter extends ArrayAdapter<Clothes> {
    Context context;
    int resource;
    List<Clothes> objects;

    public CustomAdapter(Context context, int resource, List<Clothes> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }



    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item = inflater.inflate(this.resource, null); //resource la item minh truyen vao

        Clothes cloth = this.objects.get(position);

        TextView txtId = (TextView) item.findViewById(R.id.txtId);
        TextView txtName = (TextView) item.findViewById(R.id.txtName);
        TextView txtDescription = (TextView) item.findViewById(R.id.txtDescription);
        TextView txtBrand = (TextView) item.findViewById(R.id.txtBrand);

//        txtId.setText(cloth.getId());
//        txtName.setText(cloth.getName());
//        txtDescription.setText(cloth.getDescription());
//        txtBrand.setText(cloth.getBrand());

        txtId.setText(StringUtils.capitalize(cloth.getId().toLowerCase().trim()));
        txtName.setText(StringUtils.capitalize(cloth.getName().toLowerCase().trim()));
        txtDescription.setText(StringUtils.capitalize(cloth.getDescription().toLowerCase().trim()));
        txtBrand.setText(StringUtils.capitalize(cloth.getBrand().toLowerCase().trim()));


        return item;
    }

}
