package trandiemquynh.demo_clothes.Controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import trandiemquynh.demo_clothes.Database.SqlManager;
import trandiemquynh.demo_clothes.Model.Clothes;
import trandiemquynh.demo_clothes.R;

public class ShowClothes extends AppCompatActivity {

    private EditText etID, etName, etDescription, etBrand, etStatus;
    private ImageView imageViewShow;
    SqlManager sql = new SqlManager(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_clothes);
        getSupportActionBar().setTitle("Item's information");
//        ActionBar mActionBar = getSupportActionBar();
//        mActionBar.setDisplayShowHomeEnabled(false);
//        mActionBar.setDisplayShowTitleEnabled(false);

        etID = (EditText)findViewById(R.id.etID);
        etName = (EditText)findViewById(R.id.etName);
        etDescription = (EditText)findViewById(R.id.etDescription);
        etBrand = (EditText)findViewById(R.id.etBrand);
        etStatus = (EditText)findViewById(R.id.etStatus);
        imageViewShow = (ImageView) findViewById(R.id.imageViewShow);

        Clothes cloth = getIntent().getExtras().getParcelable("Data");
//        Log.d("trandiemquynh", "status: " + cloth.getStatus());
        showEditCloth(cloth);

        Retrieve(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/"+etID.getText()+".jpg");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ShowClothes.this,MainActivity.class);
        startActivity(intent);
    }

    public void Retrieve(String filepath)
    {
        File imageFile = new File(filepath);
        if(imageFile.exists()){
            Bitmap image = BitmapFactory.decodeFile(filepath);
            // Rotate Image if Needed
            try{
                // Determine Orientation
                ExifInterface exif = new ExifInterface(filepath);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                // Determine Rotation
                int rotation = 0;
                if      (orientation == 6)      rotation = 90;
                else if (orientation == 3)      rotation = 180;
                else if (orientation == 8)      rotation = 270;
                // Rotate Image if Necessary
                if (rotation != 0){
                    // Create Matrix
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    // Rotate Bitmap
                    Bitmap rotated = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
                    // Pretend none of this ever happened!
                    image.recycle();
                    image = rotated;
                    rotated = null;
                }
            }
            catch (Exception e){}
            imageViewShow.setImageBitmap(image);
        }
        else Toast.makeText(this, "You haven't added photo for this item", Toast.LENGTH_SHORT).show();
    }

    public void showEditCloth(Clothes clothes){
        etID.setText(clothes.getId());
        etName.setText(clothes.getName());
        etDescription.setText(clothes.getDescription());
        etBrand.setText(clothes.getBrand());

        if(clothes.getStatus()==1){
            etStatus.setText("Inside the wardrobe");
        } else if(clothes.getStatus()==0){
            etStatus.setText("Outside the wardrobe");
        }
    }

    // tao button tren action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.showclothes, menu);
        return super.onCreateOptionsMenu(menu);
    }


    // xu ly button tren action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.btnExit){
            doClickExit();
        } else if(id == R.id.btnBack){
            doClickBack();
        } else if(id == R.id.btnEdit){
            doClickEdit();
        }
        return super.onOptionsItemSelected(item);
    }

    public void doClickBack(){
        Intent intent = new Intent(ShowClothes.this,ManageClothes.class);
        startActivity(intent);
        finish();
    }

    public void doClickExit(){
        Intent intent = new Intent(ShowClothes.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
    public void doClickEdit(){
        Intent intent = new Intent(ShowClothes.this,UpdateClothes.class);
        String id = etID.getText().toString();
        String name = etName.getText().toString();
        String des = etDescription.getText().toString();
        String brand = etBrand.getText().toString();
        String stt = etStatus.getText().toString();

        intent.putExtra("id",id);
        intent.putExtra("name", name);
        intent.putExtra("des", des);
        intent.putExtra("brand", brand);
        intent.putExtra("stt", stt);

        startActivity(intent);
        finish();
    }


}
