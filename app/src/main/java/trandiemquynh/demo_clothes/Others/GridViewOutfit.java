package trandiemquynh.demo_clothes.Others;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

import trandiemquynh.demo_clothes.R;

/**
 * Created by Admin on 14-Nov-17.
 */

public class GridViewOutfit  extends BaseAdapter {


    // Declare variables
    private Activity activity;
    private String[] filepath;
    private String[] filename;


    private static LayoutInflater inflater = null;

    public GridViewOutfit(Activity a, String[] fpath, String[] fname) {
        activity = a;
        filepath = fpath;
        filename = fname;

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return filepath.length;

    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.activity_grid_view_outfit, null);

        final ImageView imageView = (ImageView) vi.findViewById(R.id.image);

        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(8, 8, 8, 8); //left, top, right, bottom
//
//
//        // Decode the filepath with BitmapFactory followed by the position
//        Bitmap image = decodeSampledBitmapFromUri(filepath[position], 300, 300);
//                    //rotate
//                    try{
//                        // Determine Orientation
//                        ExifInterface exif = new ExifInterface(filepath[position]);
//                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//                        // Determine Rotation
//                        int rotation = 0;
//                        if      (orientation == 6)      rotation = 90;
//                        else if (orientation == 3)      rotation = 180;
//                        else if (orientation == 8)      rotation = 270;
//                        // Rotate Image if Necessary
//                        if (rotation != 0){
//                            // Create Matrix
//                            Matrix matrix = new Matrix();
//                            matrix.postRotate(rotation);
//                            // Rotate Bitmap
//                            Bitmap rotated = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
//                            // Pretend none of this ever happened!
//                            image.recycle();
//                            image = rotated;
//                            rotated = null;
//                        }
//
//                    }
//                    catch (Exception e){}
//        imageView.setImageBitmap(image);

        File f = new File(filepath[position]);
        Glide.with(activity)
                .load(f)
                .into(imageView);
        return vi;
    }





    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height
                        / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }
}
