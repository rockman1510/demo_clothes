package trandiemquynh.demo_clothes.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 26-Sep-17.
 */

public class Clothes implements Parcelable{
    private String id;
    private String name;
    private String description;
    private String brand;
    private Integer status;

    public Clothes() {
    }

    public Clothes(String id, String name, String description, String brand, Integer status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.brand = brand;
        this.status = status;
    }

    protected Clothes(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        brand = in.readString();
        status = in.readInt();
    }

    public static final Creator<Clothes> CREATOR = new Creator<Clothes>() {
        @Override
        public Clothes createFromParcel(Parcel in) {
            return new Clothes(in);
        }

        @Override
        public Clothes[] newArray(int size) {
            return new Clothes[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(brand);
        dest.writeInt(status);

    }
}
