package trandiemquynh.demo_clothes.Others;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.regex.Pattern;

import trandiemquynh.demo_clothes.R;

public class GridViewAdapter extends BaseAdapter {


    // Declare variables
    private Activity activity;
    private String[] filepath;
    private String[] filename;
    private String[] status;
    private Bitmap image = null;
    private static LayoutInflater inflater = null;
    TextView text;

    public GridViewAdapter(Activity a, String[] fpath, String[] fname, String[] stt) {
        activity = a;
        filepath = fpath;
        filename = fname;
        status = stt;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return filepath.length;

    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.activity_gird_view_adapter, null);
        // Locate the TextView in gridview_item.xml
        TextView text = (TextView) vi.findViewById(R.id.text);
        TextView txtStt = (TextView) vi.findViewById(R.id.txtStt);
        // Locate the ImageView in gridview_item.xml
        final ImageView imageView = (ImageView) vi.findViewById(R.id.image);

        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(8, 8, 8, 8);

        String filenames = filename[position];
        String[] parts = filenames.split(Pattern.quote("."));
        String part1 = parts[0]; //file name
        String part2 = parts[1]; //jpg

        // Set file name to the TextView followed by the position
//        txtStt.setText(status[position]);
        if(status[position].equals("1")){
            txtStt.setText("Inside");
        } else {
            txtStt.setText("Outside");
        }
        text.setText(part1);

        // Set the decoded bitmap into ImageView
        //imageView.setImageBitmap(image);

            File f = new File(filepath[position]);
            Glide.with(activity)
                    .load(f)
                    .into(imageView);

            return vi;

    }

   /* public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height
                        / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }*/

}
