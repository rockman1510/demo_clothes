package trandiemquynh.demo_clothes.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import trandiemquynh.demo_clothes.R;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setTitle("About this application");
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(About.this,MainActivity.class);
        startActivity(intent);
    }

    // tao button tren action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back, menu);
        return super.onCreateOptionsMenu(menu);
    }


    // xu ly button tren action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.btnExit){
            doClickExit();
        }
        return super.onOptionsItemSelected(item);
    }

    public void doClickExit(){
        Intent intent = new Intent(About.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
