package trandiemquynh.demo_clothes.Controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import trandiemquynh.demo_clothes.Database.SqlManager;
import trandiemquynh.demo_clothes.Model.Clothes;
import trandiemquynh.demo_clothes.Others.CustomAdapter;
import trandiemquynh.demo_clothes.R;


public class ManageClothes extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener{
    ListView listView;
    CustomAdapter customAdapter;
    SqlManager db = new SqlManager(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_clothes);
        getSupportActionBar().setTitle("Items' Information");

        ArrayList<Clothes> listClothes = db.getAllClothes();
        listView = (ListView) findViewById(R.id.listView);
        customAdapter = new CustomAdapter(ManageClothes.this, R.layout.activity_list_view, listClothes);
        listView.setAdapter(customAdapter);

        listView.setOnItemLongClickListener(this);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ManageClothes.this,MainActivity.class);
        startActivity(intent);
    }

    public void doClickDelete(int position){
        ArrayList<Clothes> listClothes = db.getAllClothes();
        Clothes cloth = listClothes.get(position);
        String id = cloth.getId();
        db.deleteCloth(id);
        File file= new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/"+cloth.getId()+".jpg");
        if(file.exists())
        {
            file.delete();
        }
        Toast.makeText(this, "Deleted successfully!", Toast.LENGTH_SHORT).show();
        listClothes = db.getAllClothes();
        listView = (ListView) findViewById(R.id.listView);
        customAdapter = new CustomAdapter(ManageClothes.this, R.layout.activity_list_view, listClothes);
        listView.setAdapter(customAdapter);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        AlertDialog.Builder b =  new AlertDialog.Builder(ManageClothes.this);
        b.setTitle("Delete item!");
        b.setMessage("Do you want to delete this item?");
        b.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                doClickDelete(position);
            }
        });
        b.setNegativeButton("No", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){

            }});
        b.create().show();
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ArrayList<Clothes> listClothes = db.getAllClothes();
        Clothes cloth = listClothes.get(position);
        Intent intent = new Intent(ManageClothes.this, ShowClothes.class);
        intent.putExtra("Data",cloth);
        startActivity(intent);
        finish();
    }


    // tao button tren action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.manageclothes, menu);
        return super.onCreateOptionsMenu(menu);
    }
    // xu ly button tren action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.btnAddCloth) {
            doClickAdd();
        }
        else if(id == R.id.btnExit){
            doClickExit();
        }
        return super.onOptionsItemSelected(item);
    }

    public void doClickAdd(){
        Intent intent = new Intent(ManageClothes.this,AddClothes.class);
        startActivity(intent);
        finish();
    }

    public void doClickExit(){
        Intent intent = new Intent(ManageClothes.this,MainActivity.class);
        startActivity(intent);
        finish();
    }


}
