package trandiemquynh.demo_clothes.Controller;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;

import trandiemquynh.demo_clothes.Database.SqlManager;
import trandiemquynh.demo_clothes.Model.Barcodes;
import trandiemquynh.demo_clothes.Model.Clothes;
import trandiemquynh.demo_clothes.R;

public class AddClothes extends AppCompatActivity {
    private EditText etID, etName, etDescription, etBrand, etStatus;
    private ImageView imageViewUpdate;
    private boolean add = false;
    private SqlManager sql = new SqlManager(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_clothes);
        getSupportActionBar().setTitle("Add item");
//        ActionBar mActionBar = getSupportActionBar();
//        mActionBar.setDisplayShowHomeEnabled(false);
//        mActionBar.setDisplayShowTitleEnabled(false);

        etID = (EditText)findViewById(R.id.etID);
        etName = (EditText)findViewById(R.id.etName);
        etDescription = (EditText)findViewById(R.id.etDescription);
        etBrand = (EditText)findViewById(R.id.etBrand);
        etStatus = (EditText)findViewById(R.id.etStatus);

        imageViewUpdate = (ImageView) findViewById(R.id.imageViewUpdate);

        SqlManager db = new SqlManager(this);
        ArrayList<Clothes> listClothes = db.getAllClothes();

        Barcodes bc = new Barcodes();
        String[] code = bc.getGenBarcode();
        Clothes cloth = null;
        for(int i=0; i<code.length; i++){
            cloth = sql.getClothById(code[i]);
            if(cloth.getStatus()==null){
                etID.setText(code[i]);
                break;
            }
            else {}
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        File file= new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/"+etID.getText().toString()+".jpg");
        Clothes cloth = null;
        cloth = sql.getClothById(etID.getText().toString());
        if(cloth.getStatus()==null)
        {
            file.delete();
        }
        Intent intent = new Intent(AddClothes.this,ManageClothes.class);
        startActivity(intent);
    }

    public void doClickSave() {
        Clothes clothes = sql.getClothByName(etName.getText().toString());
        Pattern regex = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!]");

        if(etName.getText().toString().trim()!=null && !etName.getText().toString().trim().equals("") && !etName.getText().toString().trim().isEmpty())
        {
            String id = etID.getText().toString();
            String name = etName.getText().toString();
            String description = etDescription.getText().toString();
            String brand = etBrand.getText().toString();
            Integer status = Integer.parseInt(etStatus.getText().toString());

            Clothes cloth = new Clothes(id,name,description,brand,status);

            if(name.equalsIgnoreCase(clothes.getName()) &&
                    description.equalsIgnoreCase(clothes.getDescription()) &&
                    brand.equalsIgnoreCase(clothes.getBrand())) {
                Toast.makeText(AddClothes.this, "This item's information has already been in the database", Toast.LENGTH_SHORT).show();
            }
            else if(regex.matcher(name).find()){
                Toast.makeText(AddClothes.this, "'Name' mustn't include special characters", Toast.LENGTH_SHORT).show();
            }else if(regex.matcher(description).find()){
                Toast.makeText(AddClothes.this, "'Description' mustn't include special characters", Toast.LENGTH_SHORT).show();
            } else {
                sql.addClothes(cloth);
                Toast.makeText(getApplicationContext(), "Added successfully", Toast.LENGTH_LONG).show();
                finish();
                Intent intent = new Intent(AddClothes.this,ManageClothes.class);
                startActivity(intent);
            }
        }
        else {
            Toast.makeText(AddClothes.this, "Please type in 'Name'", Toast.LENGTH_SHORT).show();
        }



    }
    private Uri mImageUri;
    protected void takePhoto(){
        //To take picture from camera:
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try
        {
            File imagesFolder = new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes");
            imagesFolder.mkdirs();
            photo = new File(imagesFolder,etID.getText()+".jpg");
        }
        catch(Exception e){}

        mImageUri = Uri.fromFile(photo);
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        //start camera intent
        startActivityForResult(takePicture, 1);
    }

    public void grabImage(ImageView imageView)
    {
        this.getContentResolver().notifyChange(mImageUri, null);
        ContentResolver cr = this.getContentResolver();
        Bitmap bitmap;
        try
        {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
            imageView.setImageBitmap(bitmap);
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    this.grabImage(imageViewUpdate);
                    imageViewUpdate.setRotation(90);
                    imageViewUpdate.setScaleType(ImageView.ScaleType.FIT_CENTER);
                }
                break;
        }
    }
    // tao button tren action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.addclothes, menu);
        return super.onCreateOptionsMenu(menu);
    }
    // xu ly button tren action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.btnExit){
            doClickExit();
        } else if(id == R.id.btnBack) {
            doClickBack();
        } else if(id == R.id.btnSave){
            doClickSave();
        }  else if(id == R.id.btnTakePhoto) {
            takePhoto();
        }
        return super.onOptionsItemSelected(item);
    }

    public void doClickBack(){
        File file= new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/"+etID.getText().toString()+".jpg");
        Clothes cloth = null;
        cloth = sql.getClothById(etID.getText().toString());
        if(cloth.getStatus()==null)
        {
            file.delete();
        }
        Intent intent = new Intent(AddClothes.this,ManageClothes.class);
        startActivity(intent);
        finish();
    }

    public void doClickExit() {
        File file= new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/"+etID.getText().toString()+".jpg");
        Clothes cloth = null;
        cloth = sql.getClothById(etID.getText().toString());
        if(cloth.getStatus()==null)
        {
            file.delete();
        }
        Intent intent = new Intent(AddClothes.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
