package trandiemquynh.demo_clothes.Others;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import java.io.File;
import java.util.regex.Pattern;

import trandiemquynh.demo_clothes.Controller.MainActivity;
import trandiemquynh.demo_clothes.Database.SqlManager;
import trandiemquynh.demo_clothes.Model.Clothes;
import trandiemquynh.demo_clothes.R;

public class GridViewImagesList extends AppCompatActivity {
    // Declare variables
    private String[] FilePathStrings;
    private String[] FileNameStrings;
    private File[] listFile;
    private String[] status;
    private GridView grid;
    private GridViewAdapter adapter;
    private File file;
    private SqlManager db = new SqlManager(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_image_grid_view);
        getSupportActionBar().setTitle("Clothing list");
        file = new File(Environment.getExternalStorageDirectory().toString()+"/DCIM/Clothes/");

        if (file.isDirectory()) {
            listFile = file.listFiles();
            // Create a String array for FilePathStrings
            FilePathStrings = new String[listFile.length];
            // Create a String array for FileNameStrings
            FileNameStrings = new String[listFile.length];

            status = new String[listFile.length];


            for (int i = 0; i < listFile.length; i++) {
                // Get the path of the image file
                FilePathStrings[i] = listFile[i].getAbsolutePath();
                // Get the name image file
                FileNameStrings[i] = listFile[i].getName();

                String[] parts = FileNameStrings[i].split(Pattern.quote("."));
                String filename = parts[0]; //file name
                String part2 = parts[1];
//                Toast.makeText(this, filename+"", Toast.LENGTH_SHORT).show();
                Clothes cloth = db.getClothById(filename);
//                status[i] = cloth.getStatus().toString();
                if(cloth.getStatus()!=null) {
                    status[i] = cloth.getStatus().toString();
//                Toast.makeText(this, cloth.getStatus()+"", Toast.LENGTH_SHORT).show();
                } else status[i]=String.valueOf(0);

            }
        }

        grid = (GridView) findViewById(R.id.gridview);
        // Pass String arrays to GridViewAdapter Class
        adapter = new GridViewAdapter(this, FilePathStrings, FileNameStrings, status);


        // Set the GridViewAdapter to the GridViewImagesList
        grid.setAdapter(adapter);


        // Capture gridview item click
        grid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent i = new Intent();
                // Pass String arrays FilePathStrings
                i.putExtra("filepath", FilePathStrings);
                // Pass String arrays FileNameStrings
//                i.putExtra("filename", FileNameStrings);
                // Pass click position
                i.putExtra("position", position);
                setResult(RESULT_OK, i);
                finish();
            }

        });

    }


    // tao button tren action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back, menu);
        return super.onCreateOptionsMenu(menu);
    }
    // xu ly button tren action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.btnExit){
            doClickExit();
        }
        return super.onOptionsItemSelected(item);
    }

    public void doClickExit(){
        Intent intent = new Intent(GridViewImagesList.this,MainActivity.class);
        startActivity(intent);
        finish();
    }


}
