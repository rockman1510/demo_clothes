package trandiemquynh.demo_clothes.Others;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by Admin on 26-Sep-17.
 */

public class FireApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}
